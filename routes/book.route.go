package routes

import (
	bookcontroller "gin-gonic-gorm/controllers/book_controller"

	"github.com/gin-gonic/gin"
)

func bookRoute(authRoute *gin.RouterGroup) {
	//user
	route := authRoute.Group("books/")
	route.GET("/", bookcontroller.GetData)
	route.PATCH("/:id", bookcontroller.UpdateById)
	route.POST("/", bookcontroller.Store)
	route.DELETE("/:id", bookcontroller.Delete)
}
