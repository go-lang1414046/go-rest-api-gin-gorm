package routes

import (
	appconfig "gin-gonic-gorm/configs/app_config"
	filecontroller "gin-gonic-gorm/controllers/file_controller"
	docs "gin-gonic-gorm/docs"
	"gin-gonic-gorm/middleware"

	"github.com/gin-gonic/gin"
	ginSwaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title 	Tag Service API
// @version	1.0
// @description A Tag service API in Go using Gin framework
// @host 	localhost:8888
// @BasePath /api
// @Security BearerAuth
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func InitRoute(route *gin.Engine) {
	apiRoute := route.Group("api/v1")

	// config swagger
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Description = "API GO Using gin gonic gorm"
	docs.SwaggerInfo.Title = "Rest API GO"

	//route swagger
	route.GET("/docs/*any", ginSwagger.WrapHandler(ginSwaggerFiles.Handler))

	authMiddlewareRoute := apiRoute.Group("/", middleware.AuthMiddleWare)

	//route static public
	route.Static("/public", appconfig.PUBLIC_STATIC)

	//Auth
	authRoute(apiRoute)

	//user
	userRoute(authMiddlewareRoute)

	//book
	bookRoute(authMiddlewareRoute)

	authMiddlewareRoute.POST("/file", filecontroller.UploadFile)
	authMiddlewareRoute.DELETE("/file/:filepath", filecontroller.RemoveFile)
}
