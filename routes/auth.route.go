package routes

import (
	authcontroller "gin-gonic-gorm/controllers/auth_controller"

	"github.com/gin-gonic/gin"
)

func authRoute(apiRoute *gin.RouterGroup) {
	//user
	route := apiRoute.Group("auth")
	route.POST("/login", authcontroller.Login)
}
