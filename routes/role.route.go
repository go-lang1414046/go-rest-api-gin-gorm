package routes

import (
	rolecontroller "gin-gonic-gorm/controllers/role_controller"

	"github.com/gin-gonic/gin"
)

func roleRoute(authRoute *gin.RouterGroup) {
	//user
	route := authRoute.Group("roles/")
	route.GET("/", rolecontroller.GetData)
	route.PATCH("/:id", rolecontroller.UpdateById)
	route.POST("/", rolecontroller.Store)
	route.DELETE("/:id", rolecontroller.Delete)
}
