package routes

import (
	usercontroller "gin-gonic-gorm/controllers/user_controller"

	"github.com/gin-gonic/gin"
)

func userRoute(authRoute *gin.RouterGroup) {
	//user
	route := authRoute.Group("users/")
	route.GET("/", usercontroller.GetData)
	route.PATCH("/:id", usercontroller.UpdateById)
	route.POST("/", usercontroller.Store)
	route.DELETE("/:id", usercontroller.Delete)
}
