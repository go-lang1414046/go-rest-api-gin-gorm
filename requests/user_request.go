package requests

import (
	"mime/multipart"
	"time"
)

type UserRequest struct {
	Name     string                `form:"name" binding:"required"`
	RoleId   int                   `form:"role_id"   binding:"required"`
	Email    string                `form:"email"  binding:"required,email"`
	Address  string                `form:"address"  binding:"required"`
	BornDate time.Time             `form:"born_date"  binding:"required"`
	Image    *multipart.FileHeader `form:"image"  binding:"required"`
	Password string                `form:"password" binding:"required"`
}
