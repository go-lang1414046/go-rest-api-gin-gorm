package requests

type BookRequest struct {
	Title       string `json:"name" from:"name" binding:"required"`
	CategoryId  int    `json:"role_id" from:"role_id"  binding:"required"`
	Description string `json:"email" from:"description"  binding:"required,email"`
}
