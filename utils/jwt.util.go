package utils

import (
	"fmt"

	"github.com/golang-jwt/jwt/v5"
)

var secret_key = "SECRET_KEY"

func GenerateToken(claims *jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(secret_key))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func VerifyToken(token string) (*jwt.Token, error) {
	tokenJwt, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		_, isValid := t.Method.(*jwt.SigningMethodHMAC)
		if !isValid {

			return nil, fmt.Errorf("unexpected signing method %s", t.Header["alg"])
		}

		return []byte(secret_key), nil
	})

	if err != nil {
		return nil, err
	}

	return tokenJwt, nil
}

func DecodeToken(token string) (jwt.MapClaims, error) {
	tokenJwt, err := VerifyToken(token)
	if err != nil {
		return nil, err
	}

	claims, isOk := tokenJwt.Claims.(jwt.MapClaims)
	if isOk && tokenJwt.Valid {
		return claims, nil
	}
	return nil, fmt.Errorf("Invalid token")
}
