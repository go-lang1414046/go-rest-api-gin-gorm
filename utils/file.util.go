package utils

import (
	"fmt"
	logconfig "gin-gonic-gorm/configs/log_config"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func SaveFile(ctx *gin.Context, fileHeader *multipart.FileHeader, path string) string {
	// Retrieve file information
	extension := filepath.Ext(fileHeader.Filename)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := uuid.New().String() + extension

	logconfig.CreateFolderIfNotExist(fmt.Sprintf("./public/%s/%s", path, newFileName))

	errUpload := ctx.SaveUploadedFile(fileHeader, fmt.Sprintf("./public/%s/%s", path, newFileName))

	if errUpload != nil {
		return ""
	} else {
		return fmt.Sprintf("%s/%s", path, newFileName)
	}
}

func DeleteFile(filePath string) error {
	err := os.Remove(filePath)
	if err != nil {
		log.Println("Failed to remove file")
		return err
	}
	return nil
}
