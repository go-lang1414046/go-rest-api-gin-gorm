package responses

import "time"

// * for return null
type UserResponse struct {
	ID       *int          `json:"id"`
	Name     *string       `json:"name"`
	Address  *string       `json:"address"`
	Email    *string       `json:"email"`
	RoleId   *int          `json:"role_id"`
	BornDate *time.Time    `json:"born_date"`
	Role     *RoleResponse `json:"role" gorm:"foreignKey:RoleId"`
}
