package responses

// * for return null
type BookResponse struct {
	ID         *int    `json:"id"`
	Title      *string `json:"title"`
	Image      *string `json:"image"`
	CategoryId *string `json:"category_id"`
}
