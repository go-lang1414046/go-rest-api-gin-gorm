package responses

type RoleResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
