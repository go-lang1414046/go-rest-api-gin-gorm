package models

type Book struct {
	ID          *int    `json:"id"`
	Title       *string `json:"title"`
	image       *string `json:"image"`
	Description *string `json:"description"`
	CategoryId  *int    `json:"category_id`
}
