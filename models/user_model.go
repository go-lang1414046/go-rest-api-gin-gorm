package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID       *int       `json:"id"`
	Name     *string    `json:"name"`
	Address  *string    `json:"address"`
	Email    *string    `json:"email"`
	RoleId   *int       `json:"role_id"`
	Image    *string    `json:"image"`
	BornDate *time.Time `json:"born_date"`
	Password *string    `json:"password"`
}

// get users
func GetData(db *gorm.DB, User *[]User) (err error) {
	err = db.Find(User).Error
	if err != nil {
		return err
	}
	return nil
}

// get user by id
func GetDataById(db *gorm.DB, User *User, id int) (err error) {
	err = db.Where("id = ?", id).First(User).Error
	if err != nil {
		return err
	}
	return nil
}
