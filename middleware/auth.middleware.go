package middleware

import (
	"gin-gonic-gorm/utils"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func AuthMiddleWare(ctx *gin.Context) {
	token := ctx.GetHeader("Authorization")

	if !strings.Contains(token, "Bearer") {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"message": "unauthenticated",
		})
		return
	}

	tokenJwt := strings.Replace(token, "Bearer ", "", -1)
	claimsData, err := utils.DecodeToken(tokenJwt)

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"message": "unauthenticated",
		})
		return
	}

	ctx.Set("claimsData", claimsData)
	ctx.Set("user_id", claimsData["id"])

	ctx.Next()
}
