package bookcontroller

import (
	"gin-gonic-gorm/database"
	"gin-gonic-gorm/models"
	"gin-gonic-gorm/requests"
	"gin-gonic-gorm/responses"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetData(ctx *gin.Context) {
	start := 0
	limit := 10

	if ctx.Query("start") != "" {
		start, _ = strconv.Atoi(ctx.Query("start"))
	}

	if ctx.Query("limit") != "" {
		limit, _ = strconv.Atoi(ctx.Query("limit"))
	}

	data := new([]models.Book)
	err := database.DB.Table("books").Offset(start).Limit(limit).Find(&data).Error
	count := int64(0)

	// database.DB.Table("books").Count(&books)
	if err != nil {
		ctx.AbortWithStatusJSON(500, gin.H{
			"message": "bad request, some field not valid!",
		})
		return
	}

	ctx.JSON(200, gin.H{
		"data":  data,
		"count": count,
	})
}

func GetDataById(ctx *gin.Context) {
	id := ctx.Param("id")
	data := new(responses.BookResponse)
	err := database.DB.Table("books").Where("id=?", id).Find(&data).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"data":    nil,
			"message": "Internet server error",
		})
		return
	}
	if data.ID == nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": "Data not found",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "",
		"data":    data,
	})

}

func Store(ctx *gin.Context) {
	dataRequest := new(requests.BookRequest)
	if err := ctx.ShouldBind(&dataRequest); err != nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": err.Error(),
		})
	}

	data := new(models.Book)
	data.Title = &dataRequest.Title
	data.Description = &dataRequest.Description
	data.CategoryId = &dataRequest.CategoryId

	err := database.DB.Table("books").Create(&data).Error

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Can't create data",
			"data":    data,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "data create success",
		"data":    data,
	})

}

func UpdateById(ctx *gin.Context) {
	id := ctx.Param("id")
	dataRequest := new(requests.BookRequest)

	if err := ctx.ShouldBind(&dataRequest); err != nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": err.Error(),
		})
	}
	data := new(models.Book)

	errData := database.DB.Table("books").Where("id = ?", id).Find(&data).Error

	if errData != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Data not found",
		})
		return
	}

	data.Title = &dataRequest.Title
	data.Description = &dataRequest.Description
	data.CategoryId = &dataRequest.CategoryId

	err := database.DB.Table("books").Updates(&data).Error

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Can't create data",
			"data":    data,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "data create success",
		"data":    data,
	})

}

func Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Required ID",
		})
		return
	}

	err := database.DB.Unscoped().Where("id = ?", id).Delete(&models.Book{}).Error

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "internal server error",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "data delete success",
	})
	return

}
