package authcontroller

import (
	"gin-gonic-gorm/database"
	"gin-gonic-gorm/models"
	"gin-gonic-gorm/requests"
	"gin-gonic-gorm/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
)

func Login(ctx *gin.Context) {
	LogiRequest := new(requests.AuthRequest)
	if err := ctx.ShouldBind(&LogiRequest); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	user := new(models.User)
	errUser := database.DB.Table("users").Where("email = ? AND password = ? ", LogiRequest.Email, LogiRequest.Password).Find(&user).Error

	if errUser != nil || user.ID == nil {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"message": "User not found",
		})
		return
	}
	claims := jwt.MapClaims{
		"id":    user.ID,
		"name":  user.Name,
		"email": user.Email,
		"exp":   time.Now().Add(time.Hour * 24).Unix(),
	}

	token, err := utils.GenerateToken(&claims)

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"message": "User not found",
		})
		return
	}
	ctx.AbortWithStatusJSON(http.StatusOK, gin.H{
		"message": "Login success",
		"token":   token,
	})

}
