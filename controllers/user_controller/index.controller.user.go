package usercontroller

import (
	"gin-gonic-gorm/database"
	"gin-gonic-gorm/models"
	"gin-gonic-gorm/requests"
	"gin-gonic-gorm/responses"
	"gin-gonic-gorm/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Headers godoc
// @Summary		Get Data Users
// @Description get users
// @Accept  	application/json
// @Produce 	application/json
// @Tags		users
// @Param   	start     query    int     		true     	"Start"		default(0)
// @Param   	limit     query    int     		true		"Limit"		default(10)
// @Param   	keyword   query    string     	false		"Keyword"
// @Success 	200 {string} string	"ok"
// @Router 		/users/ [get]
// @Security BearerAuth
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func GetData(ctx *gin.Context) {
	start := 0
	limit := 10

	if ctx.Query("start") != "" {
		start, _ = strconv.Atoi(ctx.Query("start"))
	}

	if ctx.Query("limit") != "" {
		limit, _ = strconv.Atoi(ctx.Query("limit"))
	}

	users := new([]responses.UserResponse)
	err := database.DB.Table("users").Preload("Role").Offset(start).Limit(limit).Find(&users).Error
	count := int64(0)

	// claimsData := ctx.MustGet("claimsData").(jwt.MapClaims)
	// fmt.Println("claimsData %s", claimsData)
	// database.DB.Table("users").Count(&users)
	if err != nil {
		ctx.AbortWithStatusJSON(500, gin.H{
			"message": "bad request, some field not valid!",
		})
		return
	}

	ctx.JSON(200, gin.H{
		"data":  users,
		"count": count,
	})
}

func GetDataById(ctx *gin.Context) {
	id := ctx.Param("id")
	data := new(responses.UserResponse)
	err := database.DB.Table("users").Preload("Role").Where("id=?", id).Find(&data).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"data":    nil,
			"message": "Internet server error",
		})
		return
	}
	if data.ID == nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": "Data not found",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "",
		"data":    data,
	})

}

func Store(ctx *gin.Context) {
	dataRequest := new(requests.UserRequest)
	file, _ := ctx.FormFile("image")
	if err := ctx.ShouldBind(&dataRequest); err != nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": err.Error(),
		})
		return
	}

	dataEmailExist := new(models.User)
	dataExistError := database.DB.Table("users").Where("email = ?", dataRequest.Email).Find(&dataEmailExist).Error

	if dataExistError != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internet server error",
		})
		return
	}

	if dataEmailExist.Email != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Email already exist",
			"data":    dataRequest,
		})
		return
	}

	fileName := utils.SaveFile(ctx, file, "users")

	if fileName == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Image uploaded failed",
		})
		return
	}
	password, _ := utils.HashPassword(dataRequest.Password)
	data := new(models.User)
	data.Name = &dataRequest.Name
	data.Email = &dataRequest.Email
	data.Address = &dataRequest.Address
	data.RoleId = &dataRequest.RoleId
	data.BornDate = &dataRequest.BornDate
	data.Image = &fileName
	data.Password = &password

	err := database.DB.Table("users").Create(&data).Error

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Can't create data",
			"data":    data,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "data create success",
		"data":    data,
	})

}

func UpdateById(ctx *gin.Context) {
	id := ctx.Param("id")
	dataRequest := new(requests.UserRequest)

	if err := ctx.ShouldBind(&dataRequest); err != nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": err.Error(),
		})
	}
	data := new(models.User)

	errData := database.DB.Table("users").Where("id = ?", id).Find(&data).Error

	if errData != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Data not found",
		})
		return
	}

	dataEmailExist := new(models.User)
	errEmailExist := database.DB.Table("users").Where("email = ? AND id != ?", dataRequest.Email, id).Find(&dataEmailExist).Error

	if errEmailExist != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internet server error",
		})
		return
	}

	if dataEmailExist.Email != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Email already exist",
			"data":    dataEmailExist,
		})
		return
	}

	data.Name = &dataRequest.Name
	data.Email = &dataRequest.Email
	data.Address = &dataRequest.Address
	data.RoleId = &dataRequest.RoleId
	data.BornDate = &dataRequest.BornDate

	err := database.DB.Table("users").Updates(&data).Error

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Can't create data",
			"data":    data,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "data create success",
		"data":    data,
	})

}

func Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Required ID",
		})
		return
	}

	err := database.DB.Unscoped().Where("id = ?", id).Delete(&models.User{}).Error

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "internal server error",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "data delete success",
	})
	return

}
