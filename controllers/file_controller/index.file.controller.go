package filecontroller

import (
	"gin-gonic-gorm/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func UploadFile(ctx *gin.Context) {
	file, errFile := ctx.FormFile("file")

	if errFile != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "File required",
		})
		return
	}

	valid := utils.SaveFile(ctx, file, "files")

	if valid == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "File uploaded failed",
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "file Uploaded",
	})
}

func RemoveFile(ctx *gin.Context) {
	filePath := ctx.Query("filepath")
	err := utils.DeleteFile(filePath)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "File deleted failed",
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "file deleted",
	})
}
