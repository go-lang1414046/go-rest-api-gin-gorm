package usercontroller

import (
	"gin-gonic-gorm/database"
	"gin-gonic-gorm/models"
	"gin-gonic-gorm/requests"
	"gin-gonic-gorm/responses"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetData(ctx *gin.Context) {
	start := 0
	limit := 10

	if ctx.Query("start") != "" {
		start, _ = strconv.Atoi(ctx.Query("start"))
	}

	if ctx.Query("limit") != "" {
		limit, _ = strconv.Atoi(ctx.Query("limit"))
	}

	users := new([]models.User)
	err := database.DB.Table("users").Offset(start).Limit(limit).Find(&users).Error
	count := int64(0)

	// database.DB.Table("users").Count(&users)
	if err != nil {
		ctx.AbortWithStatusJSON(500, gin.H{
			"message": "bad request, some field not valid!",
		})
		return
	}

	ctx.JSON(200, gin.H{
		"data":  users,
		"count": count,
	})
}

func GetDataById(ctx *gin.Context) {
	id := ctx.Param("id")
	data := new(responses.UserResponse)
	err := database.DB.Table("users").Where("id=?", id).Find(&data).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"data":    nil,
			"message": "Internet server error",
		})
		return
	}
	if data.ID == nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": "Data not found",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "",
		"data":    data,
	})

}

func Store(ctx *gin.Context) {
	dataRequest := new(requests.UserRequest)
	if err := ctx.ShouldBind(&dataRequest); err != nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": err.Error(),
		})
	}

	dataEmailExist := new(models.User)
	errEmailExist := database.DB.Table("users").Where("email = ?", dataRequest.Email).First(&dataEmailExist).Error

	if errEmailExist != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internet server error",
		})
		return
	}

	if dataEmailExist != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Email already exist",
			"data":    dataRequest,
		})
		return
	}

	data := new(models.User)
	data.Name = &dataRequest.Name
	data.Email = &dataRequest.Email
	data.Address = &dataRequest.Address
	data.RoleId = &dataRequest.RoleId
	data.BornDate = &dataRequest.BornDate

	err := database.DB.Table("users").Create(&data).Error

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Can't create data",
			"data":    data,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "data create success",
		"data":    data,
	})

}

func UpdateById(ctx *gin.Context) {
	id := ctx.Param("id")
	dataRequest := new(requests.UserRequest)

	if err := ctx.ShouldBind(&dataRequest); err != nil {
		ctx.JSON(http.StatusFound, gin.H{
			"data":    nil,
			"message": err.Error(),
		})
	}
	data := new(models.User)

	errData := database.DB.Table("users").Where("id = ?", id).Find(&data).Error

	if errData != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Data not found",
		})
		return
	}

	dataEmailExist := new(models.User)
	errEmailExist := database.DB.Table("users").Where("email = ? AND id != ?", dataRequest.Email, id).Find(&dataEmailExist).Error

	if errEmailExist != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internet server error",
		})
		return
	}

	if dataEmailExist.Email != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Email already exist",
			"data":    dataEmailExist,
		})
		return
	}

	data.Name = &dataRequest.Name
	data.Email = &dataRequest.Email
	data.Address = &dataRequest.Address
	data.RoleId = &dataRequest.RoleId
	data.BornDate = &dataRequest.BornDate

	err := database.DB.Table("users").Updates(&data).Error

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Can't create data",
			"data":    data,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "data create success",
		"data":    data,
	})

}

func Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Required ID",
		})
		return
	}

	err := database.DB.Unscoped().Where("id = ?", id).Delete(&models.User{}).Error

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "internal server error",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "data delete success",
	})
	return

}
