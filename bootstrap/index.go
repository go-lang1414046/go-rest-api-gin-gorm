package bootstrap

import (
	"gin-gonic-gorm/configs"
	appconfig "gin-gonic-gorm/configs/app_config"
	corsconfig "gin-gonic-gorm/configs/cors_config"
	"gin-gonic-gorm/database"
	"gin-gonic-gorm/routes"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func BootstrapApp() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	configs.InitConfig()

	database.ConnectDatabase()

	app := gin.Default()

	//CORS
	app.Use(corsconfig.CorsConfig)

	routes.InitRoute(app)
	app.Run(appconfig.PORT)
}
