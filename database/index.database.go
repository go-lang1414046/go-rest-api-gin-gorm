package database

import (
	"fmt"
	dbconfigs "gin-gonic-gorm/configs/db_configs"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDatabase() {
	var errConnection error

	if dbconfigs.DB_DRIVER == "mysql" {
		dsnMysql := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbconfigs.DB_USER, dbconfigs.DB_PASSWORD, dbconfigs.DB_HOST, dbconfigs.DB_PORT, dbconfigs.DB_NAME)
		DB, errConnection = gorm.Open(mysql.Open(dsnMysql), &gorm.Config{})
	}

	// if dbconfigs.DB_DRIVER == "pgsql" {
	// 	dsnMysql := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4_general_ci&parseTime=True&loc=Local", dbconfigs.DB_USER, dbconfigs.DB_PASSWORD, dbconfigs.DB_HOST, dbconfigs.DB_PORT, dbconfigs.DB_NAME)
	// 	db, errConnection = gorm.Open(mysql.Open(dsnMysql), &gorm.Config{})
	// }

	if errConnection != nil {
		panic("Can't connect to database")

	}

	log.Println("Connected database")
}
