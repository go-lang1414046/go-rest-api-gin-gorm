CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    address TEXT,
    role_id int NOT NULL,
    born_date DATE NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    image VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    FOREIGN KEY (role_id) REFERENCES roles(id)
)

-- migrate -database mysql://localhost:3307/gin_gonic_gorm_web_framework -path database/migrations up
--  migrate -database "mysql://root:@127.0.0.1:3307/gin_gonic_gorm_web_framework" -path database/migrations up
-- migrate -database "mysql://root@tcp(localhost:3307)/gin_gonic_gorm_web_framework" -path database/migrations up
