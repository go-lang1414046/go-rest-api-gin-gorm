package configs

import (
	appconfig "gin-gonic-gorm/configs/app_config"
	dbconfigs "gin-gonic-gorm/configs/db_configs"
	logconfig "gin-gonic-gorm/configs/log_config"
)

func InitConfig() {
	logconfig.DefaultLogging()
	appconfig.InitAppConfig()
	dbconfigs.InitDBConfig()
}
