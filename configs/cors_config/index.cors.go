package corsconfig

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func CorsConfig(ctx *gin.Context) {
	ctx.Writer.Header().Set("Access-Controll-Allow-Origin", "*")
	ctx.Writer.Header().Set("Access-Controll-Allow-Credential", "true")
	ctx.Writer.Header().Set("Access-Controll-Allow-Headers", "Content-Type, Content-Length, Authorization")
	ctx.Writer.Header().Set("Access-Controll-Allow-Methods", "POST,DELETE,GET,PACTH,OPTIONS,PUT")
	if ctx.Request.Method == "Options" {
		ctx.AbortWithStatus(http.StatusNoContent)
		return
	}
	ctx.Next()

}
