package logconfig

import (
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

var defaultLogFilePath = "logs/gin.log"

func CreateFolderIfNotExist(path string) {
	dir := filepath.Dir(path)

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.MkdirAll(dir, 0644)
		if err != nil {
			log.Println("Failed to create")
		}
	}
}

func OpenOrCreateFile(path string) (*os.File, error) {
	logFile, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		logFile, errCreateFile := os.Create(path)
		if errCreateFile != nil {
			log.Println("Cant create file")
		}
		return logFile, nil
	}
	return logFile, nil
}

func DefaultLogging() {
	log.Println("DefaultLogging")
	gin.DisableConsoleColor()
	CreateFolderIfNotExist(defaultLogFilePath)
	f, err := OpenOrCreateFile(defaultLogFilePath)
	if err != nil {
		log.Println("Cant create file openOrCreateLogFile")
	}
	gin.DefaultErrorWriter = io.MultiWriter(f)
	log.SetOutput(gin.DefaultWriter)

}
