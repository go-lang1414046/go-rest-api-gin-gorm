package dbconfigs

import (
	"os"
)

var DB_DRIVER = "mysql"
var DB_HOST = "localhost"
var DB_PORT = "3307"
var DB_NAME = "gin_gonic_gorm_web_framework"
var DB_USER = "root"
var DB_PASSWORD = ""

func InitDBConfig() {
	driverEnv := os.Getenv("DB_DRIVER")
	hostEnv := os.Getenv("DB_HOST")
	portEnv := os.Getenv("DB_PORT")
	nameEnv := os.Getenv("DB_NAME")
	userEnv := os.Getenv("DB_USER")
	passwordEnv := os.Getenv("DB_PASSWORD")

	if driverEnv != "" {
		DB_DRIVER = driverEnv
	}
	if hostEnv != "" {
		DB_HOST = hostEnv
	}
	if portEnv != "" {
		DB_PORT = portEnv
	}

	if userEnv != "" {
		DB_USER = userEnv
	}
	if nameEnv != "" {
		DB_NAME = nameEnv
	}
	if passwordEnv != "" {
		DB_PASSWORD = passwordEnv
	}
}
