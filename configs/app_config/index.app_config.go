package appconfig

import "os"

var PORT = ":8000"
var APP_DOMAIN = "localhost:8080"
var PUBLIC_STATIC = "/public"

func InitAppConfig() {
	portEnv := os.Getenv("APP_PORT")
	if portEnv != "" {
		PORT = portEnv
	}
	publicStaticEnv := os.Getenv("PUBLIC_STATIC")
	if publicStaticEnv != "" {
		PUBLIC_STATIC = publicStaticEnv
	}
	appDomain := os.Getenv("APP_DOMAIN")
	if appDomain != "" {
		APP_DOMAIN = appDomain
	}
}
